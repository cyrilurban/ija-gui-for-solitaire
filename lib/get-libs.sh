#!/bin/bash


ZIP_FILE="`find ./ -maxdepth 1 | grep master`"

if [[ $ZIP_FILE == "" ]]; then
	wget https://bitbucket.org/xsolci00/solitaire-data/get/master.zip
fi

ZIP_FILE="`find ./ -maxdepth 1 | grep master`"

unzip $ZIP_FILE

FOLDER="`find ./ -maxdepth 1 | grep solitaire-data`"

mv $FOLDER/* .
rmdir $FOLDER
rm $ZIP_FILE
