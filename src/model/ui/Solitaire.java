/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.board.AbstractFactorySolitaire;
import model.board.FactoryKlondike;
import model.cards.Card;
import model.cards.CardDeck;
import model.cards.CardPack;
import model.cards.CardStack;
import model.cards.StartPack;


/**
 *
 * @author cyril
 */
public class Solitaire extends Application {
    
    // get screen info (dynamic for another screen width and height)
    static Screen screen = Screen.getPrimary();
    static Rectangle2D bounds = screen.getVisualBounds();
    
    // constants for GUI (dynamic for another screen width and height)
    // gets from screen info
    public static double WINDOW_WIDTH = bounds.getWidth();
    public static double WINDOW_HEIGHT = bounds.getHeight();   
    public static double TOP_SIZE = WINDOW_WIDTH/30; 
    public static double TILE_SIZE = WINDOW_WIDTH/16.86;   
    public static double CARD_WIDTH = WINDOW_WIDTH/10.31; 
    public static double CARD_HEIGHT = WINDOW_HEIGHT/4.19;
    public static double CARDS_UNDER = WINDOW_HEIGHT/26.4;
    public static double SPACE_BETWEEN_CARDS = WINDOW_WIDTH/30.91; 
    
    // object for GUI (for orientation)
    private final Ground[] SPGround = new Ground[4];
    private final Ground[] CPGround = new Ground[4];   
    private final Ground[][] TPGround = new Ground[4][4];
    private final Ground[][] WPGround = new Ground[4][7];
    
    // child for root (GUI)
    Pane root = new Pane();
    Pane game[] = new Pane[4];   
    private Group board[] = new Group[4];
    private StackPane winHolder = new StackPane();
    private Label[] hintLabels = new Label[4]; 
    
    // objects for logic
    private CardDeck[][] TP = new CardDeck[4][4];
    private CardStack[][] WP = new CardStack[4][7];   
    private StartPack[] SP = new StartPack[4];   
    private CardPack[] CP = new CardPack[4];
      
    // factory for call init function
    protected AbstractFactorySolitaire factory = new FactoryKlondike();

    // for mouse events
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    
    // undo
    String[][] undo1 = new String[4][7];
    
    // geting card info
    private int[] sourcePack = new int[4]; 
    private Card[] getingCard = new Card[4];
    private CardStack[] getingStack = new CardStack[4];
    private boolean[] multyGetFlag = new boolean[4];    
    private int[] getingPosition = new int[4];
    
    boolean flagBig = true;
    int numberOfGame = 0;
    int pressedGame = 0;
    
    /**
     * Set the body.
     * @return 
     */
    private Parent createContent() {
               
        // set background
        StackPane holder = new StackPane();
        Canvas canvas = new Canvas(WINDOW_WIDTH, WINDOW_HEIGHT);
        holder.getChildren().add(canvas);
        root.getChildren().add(holder);
        holder.setStyle("-fx-background-color: green");
        
        // games
        for(int i = 0; i< 4; i++) { 
            Pane game1 = new Pane();
            game[i] = game1;
        }

        root.getChildren().addAll(game[0]);  
        
        game[0].setLayoutX(0);
        game[0].setLayoutY(0);
        game[1].setLayoutX(WINDOW_WIDTH/2);
        game[1].setLayoutY(0);
        game[2].setLayoutX(0);
        game[2].setLayoutY(WINDOW_HEIGHT/2);
        game[3].setLayoutX(WINDOW_WIDTH/2);
        game[3].setLayoutY(WINDOW_HEIGHT/2);
        
        
        // set buttons on menu
        VBox vBox = new VBox();
        vBox.setPrefWidth(100);
        
        // exit
        Button[] exitButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {       
            Button exitButton = new Button("exit");
            exitButton.setOnMousePressed(pressExit);
            exitButton.setLayoutX(0);
            exitButton.setLayoutY(0);
            exitButton.setMinWidth(vBox.getPrefWidth());
            exitButtons[i] = exitButton;
        }
        
        // new game
        Button[] newGameButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {  
            Button newGameButton = new Button("new game");
            newGameButton.setOnMousePressed(pressNewGame);
            newGameButton.setLayoutX(100);
            newGameButton.setLayoutY(0);
            newGameButton.setMinWidth(vBox.getPrefWidth());
            newGameButtons[i] = newGameButton;
        }
        
        // save
        Button[] saveButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {  
            Button saveButton = new Button("save");
            saveButton.setOnMousePressed(saveGame);
            saveButton.setLayoutX(200);
            saveButton.setLayoutY(0);
            saveButton.setMinWidth(vBox.getPrefWidth());
            saveButtons[i] = saveButton;
        }
        
        // load
        Button[] loadButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {  
            Button loadButton = new Button("load");
            loadButton.setOnMousePressed(loadGame);
            loadButton.setLayoutX(300);
            loadButton.setLayoutY(0);
            loadButton.setMinWidth(vBox.getPrefWidth());
            loadButtons[i] = loadButton;
        }
        
        // undo
        Button[] undoButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {  
            Button undoButton = new Button("undo");
            undoButton.setOnMousePressed(undo);
            undoButton.setLayoutX(400);
            undoButton.setLayoutY(0);
            undoButton.setMinWidth(vBox.getPrefWidth());
            undoButtons[i] = undoButton;
        }
        
        // add game
        Button[] addButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {  
            Button addButton = new Button("add game");
            addButton.setOnMousePressed(add);
            addButton.setLayoutX(500);
            addButton.setLayoutY(0);
            addButton.setMinWidth(vBox.getPrefWidth());
            addButtons[i] = addButton;            
        }
        
        // hint game
        Button[] hintButtons = new Button[4]; 
        for(int i = 0; i< 4; i++) {  
            Button hintButton = new Button("hint");
            hintButton.setOnMousePressed(hint);
            hintButton.setLayoutX(600);
            hintButton.setLayoutY(0);
            hintButton.setMinWidth(vBox.getPrefWidth());
            hintButtons[i] = hintButton;            
        }
        
        // label for hint
        for(int i = 0; i< 4; i++) {  
            Label hintLabel = new Label("");
            
            hintLabel.setStyle("-fx-font-size: 22px; -fx-text-fill: midnightblue;");
            hintLabel.setEffect(new Glow());
            hintLabel.setMaxWidth(250);
            
            hintLabel.setLayoutX(720);
            hintLabel.setLayoutY(0);
            hintLabels[i] = hintLabel;            
        }
        
        // board (one game)
        for(int i = 0; i< 4; i++) { 
            Group boardTmp = new Group();
            board[i] = boardTmp;
        }
                           
        // set won panel
        Canvas winCanvas = new Canvas(500, 200);
        winHolder.getChildren().add(winCanvas);
        root.getChildren().add(winHolder);      
        winHolder.setStyle("-fx-background-color: gold");
        winHolder.toFront();
        winHolder.setLayoutX(WINDOW_WIDTH/2 - 500/2);
        winHolder.setLayoutY(WINDOW_HEIGHT/2 - 200/2);     
        Text text1 = new Text("You won!");
        text1.setFill(Color.BLACK);
        text1.setFont(Font.font("Helvetica", FontPosture.ITALIC, 80));
        
        // button on the panel
        Button newGameButtonOnPanel = new Button("new game");
        newGameButtonOnPanel.setOnMousePressed(pressNewGame);        
        newGameButtonOnPanel.setTranslateY(70);
        newGameButtonOnPanel.setStyle("-fx-font: 22 arial; -fx-base: #b6e7c9;");
        
        winHolder.getChildren().addAll(text1, newGameButtonOnPanel);
        // hide panel
        winHolder.setVisible(false); 
        

           
        // add objects to on game         
        for(int i = 0; i< 4; i++) { 
            game[i].getChildren().addAll(board[i]);
        }
        
        // board add buttons
        for(int i = 0; i< 4; i++) {        
            board[i].getChildren().addAll(exitButtons[i], newGameButtons[i], saveButtons[i], 
                    loadButtons[i], undoButtons[i], addButtons[i], hintButtons[i], hintLabels[i]);
        }

        for(int i = 0; i< 4; i++) { 
            Ground SPGroundTmp = new Ground();
            SPGround[i] = SPGroundTmp;
            
            Ground CPGroundTmp = new Ground();
            CPGround[i] = CPGroundTmp;           
        }
        
        // initialize all packs
        initCardPack(0);
        initStartPack(0);
        initTargetPack(0);
        initWorkingPack(0);
        
        // show cards
        showStartPack(0);
        showCardPack(0);
        showTargetPack(0);
        showWorkingPack(0);
        
        // save init position of card for undo history
        undo1[0][0] = executeSave(0);
        multyGetFlag[0] = false;
        winHolder.toFront();
        
        return root;
    }
    
    /**
     * Event if press on the hint button.
     */
    EventHandler<MouseEvent> hint = 
        (MouseEvent t) -> {
            
            // decides what pack is it from location of mouse
            int pack = whatPack(t.getSceneX(), t.getSceneY());
            
            String hintResult;
            hintResult = executeHint(pack);         
            hintLabels[pack].setText(hintResult);
            //System.out.println("=================");
            //printTurnFaceUp(pack);      
    };
    
    // void printTurnFaceUp(int pack)
    // {
    //     for (int i = 0 ;i < 7; i++)
    //     {
    //         for (int j = 0;j < WP[pack][i].size ; j++)
    //         {
    //             if (WP[pack][i].cards[j] != null && WP[pack][i].cards[j].isTurnedFaceUp())
    //             {
    //                 System.out.println(WP[pack][i].cards[j]);
    //             }
                
    //         }
    //     }
    // }

    String executeHint(int pack){

        //System.out.println(WP[pack][i].cards[j].value);
        
        for (int i = 0 ;i < 7; i++) {
            for (int j = 0;j < WP[pack][i].size ; j++) {
                
                    //System.out.println(WP[pack][i].cards[j]);
                    
                for (int loop1 = 0 ;loop1 < 7 && loop1 != i ; loop1++)
                {
                    if (WP[pack][i].cards[j] != null &&
                        WP[pack][loop1].get() != null &&
                        WP[pack][i].cards[j].isTurnedFaceUp() &&
                        WP[pack][loop1].get().isTurnedFaceUp())
                    {
                        // Work Pack hint
                        if ((WP[pack][loop1].get().value - 1) == WP[pack][i].cards[j].value &&
                            WP[pack][i].cards[j].value != 1
                            )
                        {
                            if ((WP[pack][loop1].get().color == Card.Color.DIAMONDS ||
                                WP[pack][loop1].get().color == Card.Color.HEARTS)
                                &&
                                (WP[pack][i].cards[j].color == Card.Color.CLUBS ||
                                WP[pack][i].cards[j].color == Card.Color.SPADES))
                            {
                                return  WP[pack][i].cards[j] + "->" + WP[pack][loop1].get();
                            }
                            if ((WP[pack][loop1].get().color == Card.Color.CLUBS ||
                                WP[pack][loop1].get().color == Card.Color.SPADES)
                                &&
                                (WP[pack][i].cards[j].color == Card.Color.DIAMONDS ||
                                WP[pack][i].cards[j].color == Card.Color.HEARTS))
                            {
                                return  WP[pack][i].cards[j] + "->" + WP[pack][loop1].get();
                            }
                            //!!!!!!!!!!!!!!!!!
                            //return  WP[pack][i].cards[j] + "->" + WP[pack][loop1].get();

                        }
                        if (WP[pack][i].cards[j].value == 13 &&
                            WP[pack][loop1].get() == null
                            ) 
                        {
                            return WP[pack][i].cards[j] + "-> empty deck";
                        }

                        //Card Pack hint
                        if (CP[pack].get() != null &&
                            WP[pack][loop1].get() != null &&
                            (CP[pack].get().value + 1) == WP[pack][loop1].get().value &&
                            CP[pack].get().value != 1
                            )
                        {
                            if ((CP[pack].get().color == Card.Color.DIAMONDS ||
                                CP[pack].get().color == Card.Color.HEARTS)
                                &&
                                (WP[pack][loop1].get().color == Card.Color.CLUBS ||
                                WP[pack][loop1].get().color == Card.Color.SPADES))
                            {
                                return CP[pack].get() + "->" + WP[pack][loop1].get();
                            }
                            
                            if ((CP[pack].get().color == Card.Color.CLUBS ||
                                CP[pack].get().color == Card.Color.SPADES)
                                &&
                                (WP[pack][loop1].get().color == Card.Color.DIAMONDS ||
                                WP[pack][loop1].get().color == Card.Color.HEARTS))
                            {
                                return CP[pack].get() + "->" + WP[pack][loop1].get();
                            }

                            // return CP[pack].get() + "->" + WP[pack][loop1].get();
                        }
                        if (CP[pack].get() != null &&
                            WP[pack][loop1].get() == null &&
                            CP[pack].get().value == 13
                            )
                        {
                            return CP[pack].get() + "-> empty deck";
                        }
                    }        
                }
            }

            //target pack hint
            for (int targets = 0; targets < 4; targets++)
            {
                
                if (WP[pack][i].get() != null &&
                    TP[pack][targets].get() != null &&
                    (TP[pack][targets].get().value + 1) == WP[pack][i].get().value &&
                    TP[pack][targets].get().color == WP[pack][i].get().color )
                {
                    return WP[pack][i].get() + "-> target pack";
                }
                
                if (WP[pack][i].get() != null &&
                    (TP[pack][targets].get() == null) && (WP[pack][i].get().value == 1))
                {
                    return WP[pack][i].get() + "-> target pack";
                }
                if (CP[pack].get() != null &&
                    (TP[pack][targets].get() == null) && CP[pack].get().value == 1) 
                {
                    return CP[pack].get() + "->target pack";
                }
                if (CP[pack].get() != null && TP[pack][targets].get() != null &&
                    (TP[pack][targets].get().value + 1) == CP[pack].get().value &&
                    TP[pack][targets].get().color == CP[pack].get().color )
                {
                    return CP[pack].get() + "->target pack";
                }
            }
        }

        return "Try take new card";
    }

    /**
     * refresh sizes (for 4 windows)
     * @param pack 
     */
    void refresh(int pack) {
        
        SPGround[pack].view.setFitHeight(CARD_HEIGHT);
        SPGround[pack].view.setFitWidth(CARD_WIDTH);
        SPGround[pack].setLayoutX(TILE_SIZE);
        SPGround[pack].setLayoutY(TOP_SIZE);
        
        CPGround[pack].view.setFitHeight(CARD_HEIGHT);
        CPGround[pack].view.setFitWidth(CARD_WIDTH);
        CPGround[pack].setLayoutX(TILE_SIZE + CARD_WIDTH + SPACE_BETWEEN_CARDS);
        CPGround[pack].setLayoutY(TOP_SIZE);
        
        int constant = 0; //for GUI       
        int i = 0;
        for(int x = 3; x < 7; x++) {  //bad value for GUI             
            TPGround[pack][i].view.setFitHeight(CARD_HEIGHT);
            TPGround[pack][i].view.setFitWidth(CARD_WIDTH);
            TPGround[pack][i].setLayoutX(TILE_SIZE  + x * CARD_WIDTH + 3*SPACE_BETWEEN_CARDS + constant);
            constant += SPACE_BETWEEN_CARDS;
            TPGround[pack][i].setLayoutY(TOP_SIZE);
            i++;
        }
        
        constant = 0; // for GUI
        i = 0;
        for(int x = 0; x < 7; x++) {        
            WPGround[pack][i].view.setFitHeight(CARD_HEIGHT);
            WPGround[pack][i].view.setFitWidth(CARD_WIDTH);
            WPGround[pack][i].setLayoutX(TILE_SIZE  + x * CARD_WIDTH + constant);
            constant += SPACE_BETWEEN_CARDS;
            WPGround[pack][i].setLayoutY(TOP_SIZE + CARD_HEIGHT + SPACE_BETWEEN_CARDS);
            i++;
        }
    }
    
        /**
     * change size - small (for 4 windows)
     */
    void getSmaller() {
        // save cards
        String gamePossition = executeSave(0);
        
        TOP_SIZE = TOP_SIZE/2; 
        TILE_SIZE = TILE_SIZE/2;   
        CARD_WIDTH = CARD_WIDTH/2; 
        CARD_HEIGHT = CARD_HEIGHT/2;
        CARDS_UNDER = CARDS_UNDER/2;
        SPACE_BETWEEN_CARDS = SPACE_BETWEEN_CARDS/2;
        
        refresh(0);
        
        int pack = 0;
        SP[pack] = null;
        CP[pack] = null;
        for(int i = 0; i<4; i++) {
            TP[pack][i] = null;
        }
        for(int i = 0; i<7; i++) {
            WP[pack][i] = null;
        }
                
        // initialize all packs
        initCardPack(pack);
        initStartPack(pack);
        initTargetPack(pack);
        initWorkingPack(pack);

        // show cards
        showStartPack(pack);
        showCardPack(pack);
        showTargetPack(pack);
        showWorkingPack(pack);
        
        // show cards
        executeLoad(gamePossition, 0);
        
    }
    
    /**
     * change size - big (for 4 windows)
     */
    void getBigger() {
        // save cards
        String gamePossition = executeSave(0);
              
        TOP_SIZE = TOP_SIZE*2; 
        TILE_SIZE = TILE_SIZE*2;   
        CARD_WIDTH = CARD_WIDTH*2; 
        CARD_HEIGHT = CARD_HEIGHT*2;
        CARDS_UNDER = CARDS_UNDER*2;
        SPACE_BETWEEN_CARDS = SPACE_BETWEEN_CARDS*2;
        
        refresh(0);
        
        int pack = 0;
        SP[pack] = null;
        CP[pack] = null;
        for(int i = 0; i<4; i++) {
            TP[pack][i] = null;
        }
        for(int i = 0; i<7; i++) {
            WP[pack][i] = null;
        }

        // initialize all packs
        initCardPack(pack);
        initStartPack(pack);
        initTargetPack(pack);
        initWorkingPack(pack);

        // show cards
        showStartPack(pack);
        showCardPack(pack);
        showTargetPack(pack);
        showWorkingPack(pack);
           
        // show cards
        executeLoad(gamePossition, 0);
    }
    
    /**
     * Decides what pack is it from location of mouse.
     * @param X
     * @param Y
     * @return pack
     */
    int whatPack(double X, double Y) {
        int pack = 0;
            if(X < WINDOW_WIDTH/2 &&  Y < WINDOW_HEIGHT/2) {
                pack = 0;
            }
            else if(X > WINDOW_WIDTH/2 &&  Y < WINDOW_HEIGHT/2) {
                pack = 1;
            }
            else if(X < WINDOW_WIDTH/2 &&  Y > WINDOW_HEIGHT/2) {
                pack = 2;
            }
            else if(X > WINDOW_WIDTH/2 &&  Y > WINDOW_HEIGHT/2) {
                pack = 3;
            }
        
        return pack;
    }
    
    
    //TODO
    /**
     * Event if press on the exit button.
     */
    EventHandler<MouseEvent> pressExit = 
        (MouseEvent t) -> {
            
            numberOfGame--;

            // decides what pack is it from location of mouse
            int pack = whatPack(t.getSceneX(), t.getSceneY());
                       
            // no game -> exit
            if (numberOfGame < 0) {                                     
                System.exit(0);
            }
            // last game -> full screen
            else if(numberOfGame==0 && flagBig==false) { 
                root.getChildren().remove(game[pack]);

                refresh(pack);
                
                showStartPack(pack);
                showCardPack(pack);
                showTargetPack(pack);
                showWorkingPack(pack);
                
                getBigger();
                flagBig = true;
            }
            // more games -> remove
            else {
                root.getChildren().remove(game[pack]);

                refresh(pack);
                
                showStartPack(pack);
                showCardPack(pack);
                showTargetPack(pack);
                showWorkingPack(pack);
            }
            
            
    };
 
    /**
     * Event if press on the new game button.
     */
    EventHandler<MouseEvent> pressNewGame = 
        new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                
                // decides what pack is it from location of mouse
                int pack;
                if(flagBig == true) {
                    pack = 0;
                }
                else {
                   pack = whatPack(t.getSceneX(), t.getSceneY()); 
                }
                
                SP[pack] = null;
                CP[pack] = null;
                for(int i = 0; i<4; i++) {
                    TP[pack][i] = null;
                }
                for(int i = 0; i<7; i++) {
                    WP[pack][i] = null;
                }
                
                // initialize all packs
                initCardPack(pack);
                initStartPack(pack);
                initTargetPack(pack);
                initWorkingPack(pack);

                // show cards
                showStartPack(pack);
                showCardPack(pack);
                showTargetPack(pack);
                showWorkingPack(pack);
                
                winHolder.setVisible(false);

                undo1[pack][0] = null;
                undo1[pack][1] = null;
                undo1[pack][2] = null;
                undo1[pack][3] = null;
                undo1[pack][4] = null;
                undo1[pack][5] = null;
                undo1[pack][6] = null;
            }
    };
    
    
    /**
     * Event if press on the save button.
     */
    EventHandler<MouseEvent> saveGame = 
        (MouseEvent t) -> {
            
            FileChooser fileChooser = new FileChooser();
            
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("solitaire files (*.sol)", "*.sol"));
            fileChooser.setInitialFileName("*.sol");
            
            //Show save file dialog
            File file = fileChooser.showSaveDialog(null);
            
            if(file != null){
                // decides what pack is it from location of mouse
                int pack = whatPack(t.getSceneX(), t.getSceneY());

                // get all cards to string
                String text = executeSave(pack);
                SaveFile(text, file);      
            }
    };
    
    /**
     * Event if press on the load button.
     */
    EventHandler<MouseEvent> loadGame = 
        (MouseEvent t) -> {
             
            FileChooser fileChooser = new FileChooser();
            
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("solitaire files (*.sol)", "*.sol"));
            fileChooser.setInitialFileName("*.sol");
            
            //Show save file dialog
            File file = fileChooser.showOpenDialog(null);
            
            if (file != null) {
                try {
                // convert from file to string
                String inputText = String.join("\n", Files.readAllLines(Paths.get(file.toString())));
                
                // decides what pack is it from location of mouse
                int pack = whatPack(t.getSceneX(), t.getSceneY());
                
                executeLoad(inputText, pack);
                
                // save init position of card for undo history
                undo1[pack][0] = executeSave(pack);
                undo1[pack][1] = null;
                undo1[pack][2] = null;
                undo1[pack][3] = null;
                undo1[pack][4] = null;
                undo1[pack][5] = null;
                undo1[pack][6] = null;
                
                } catch (IOException ex) {
                    Logger.getLogger(Solitaire.class.getName()).log(Level.SEVERE, null, ex);               
                }
            }           
    };
    
    /**
     * Function for saving process.
     * @param content
     * @param file 
     */
    private void SaveFile(String content, File file){
        try {
            FileWriter fileWriter;
              
            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Solitaire.class
                .getName()).log(Level.SEVERE, null, ex);
        }          
    }
    
    
    /**
     * Event if press on the undo button.
     */
    EventHandler<MouseEvent> undo = 
        (MouseEvent t) -> {
            
            // decides what pack is it from location of mouse
            int pack = whatPack(t.getSceneX(), t.getSceneY());
            
            // execute undo, using function for loading (the same)
            if(undo1[pack][1] != null) {
                executeLoad(undo1[pack][1], pack);
                
                // change undo1[0] history
                undo1[pack][0] = undo1[pack][1];
                undo1[pack][1] = undo1[pack][2];
                undo1[pack][2] = undo1[pack][3];
                undo1[pack][3] = undo1[pack][4];
                undo1[pack][4] = undo1[pack][5];
                undo1[pack][5] = undo1[pack][6];
                undo1[pack][6] = null;
            }
            
    };
    
    /**
     * Event if press on the add button.
     */
    EventHandler<MouseEvent> add = 
        (MouseEvent t) -> {
                         
            if(flagBig) {
                getSmaller();
                flagBig = false;
            }

            if (numberOfGame < 3) {
                numberOfGame++;                      
                root.getChildren().addAll(game[numberOfGame]);
                
                initCardPack(numberOfGame);
                initStartPack(numberOfGame);
                initTargetPack(numberOfGame);
                initWorkingPack(numberOfGame);
                                
                showStartPack(numberOfGame);
                showCardPack(numberOfGame);
                showTargetPack(numberOfGame);
                showWorkingPack(numberOfGame);
                
                // save init position of card for undo history
                undo1[numberOfGame][0] = executeSave(numberOfGame);
                multyGetFlag[numberOfGame] = false;
                winHolder.toFront();
            }            
    };
    
    /**
     * Event if press on any card.
     */
    EventHandler<MouseEvent> cardOnMousePressedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            
            // decides what pack is it from location of mouse
            int pack;
            if(flagBig) {
                pack = 0;
            }
            else {
                pack = whatPack(t.getSceneX(), t.getSceneY());
            }
            
            pressedGame = pack;
            
            // mouse positions
            orgSceneX = t.getSceneX();
            orgSceneY = t.getSceneY();
            
            // card positions
            orgTranslateX = ((Card)(t.getSource())).getTranslateX();
            orgTranslateY = ((Card)(t.getSource())).getTranslateY();
            
            sourcePack[pack] = closest(t.getSceneX(), t.getSceneY(), pack);
            
            // target pack
            if (sourcePack[pack] < 4 && sourcePack[pack] >= 0) {
                getingCard[pack] = TP[pack][sourcePack[pack]].pop();
            }
            // working pack
            else if(sourcePack[pack] > 3 && sourcePack[pack] < 11) {
                
                Card topCard;
                topCard = WP[pack][sourcePack[pack]-4].get();
                
                // the topest card - symple transfare (one) card
                if((topCard.equals(((Card)(t.getSource())))) == true) {
                    multyGetFlag[pack] = false;
                    getingCard[pack] = WP[pack][sourcePack[pack]-4].pop();
                }
                // multy get - tranfare more cards
                else {
                    multyGetFlag[pack] = true;
                    getingStack[pack] = WP[pack][sourcePack[pack]-4].pop(((Card)(t.getSource())));
                                        
                    for(int i = 0; i < WP[pack][sourcePack[pack]-4].size; i++) {
                        if(((WP[pack][sourcePack[pack]-4].cards[i]).equals(((Card)(t.getSource())))) == true) {
                            getingPosition[pack] = i;
                            break;
                        }
                    }
                    
                    
                }
    
            }
            // card pack
            else if(sourcePack[pack] == 11) {
                getingCard[pack] = CP[pack].pop(); 
            }
        }
    };
     
    /**
     * Event if press on the card from start pack.
     */
    EventHandler<MouseEvent> pressStartPackCard = 
        new EventHandler<MouseEvent>() {
 
            @Override
            public void handle(MouseEvent t) {
                
                // decides what pack is it from location of mouse
                int pack = whatPack(t.getSceneX(), t.getSceneY());
                
                Card top;
                top = SP[pack].pop();
                top.setFaceAndImage(true);
                CP[pack].put(top);
                                
                // after press refresh cards
                showStartPack(pack);
                showCardPack(pack);
                showTargetPack(pack);
                showWorkingPack(pack);
                
                // save history for undo
                undo1[pack][6] = undo1[pack][5];
                undo1[pack][5] = undo1[pack][4];
                undo1[pack][4] = undo1[pack][3];
                undo1[pack][3] = undo1[pack][2];
                undo1[pack][2] = undo1[pack][1];
                undo1[pack][1] = undo1[pack][0];
                undo1[pack][0] = executeSave(pack);
                
                // history transfare up
                if(undo1[pack][1] == null) {
                    undo1[pack][1] = undo1[pack][0];
                }
                

                hintLabels[pack].setText("");
                
            }
    };
    
     /**
     * Event if press on the return panel.
     */
    EventHandler<MouseEvent> pressReturn = 
        new EventHandler<MouseEvent>() {
 
            @Override
            public void handle(MouseEvent t) {
                
                // decides what pack is it from location of mouse
                int pack = whatPack(t.getSceneX(), t.getSceneY());
                
                Card top;
                for(int i = 0; i<CP[pack].size; i++) {
                    top = CP[pack].pop();
                    top.setFaceAndImage(false);
                    SP[pack].put(top);
                }
                    
                // after press refresh cards
                showCardPack(pack);
                showStartPack(pack);               
            }
    };
    
    /**
     * Event if dragging with any card.
     */
    EventHandler<MouseEvent> cardOnMouseDraggedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
            // offset for better view            
            double offsetX = t.getSceneX() - orgSceneX;
            double offsetY = t.getSceneY() - orgSceneY;
            
            // positions from press event + offset
            double newTranslateX = orgTranslateX + offsetX;
            double newTranslateY = orgTranslateY + offsetY;      
            
            if(multyGetFlag[pressedGame] == true) {
                
                for(int i = getingPosition[pressedGame]; i < WP[pressedGame][sourcePack[pressedGame]-4].size; i++) {
                    WP[pressedGame][sourcePack[pressedGame]-4].cards[i].setTranslateX(newTranslateX);
                    WP[pressedGame][sourcePack[pressedGame]-4].cards[i].setTranslateY(newTranslateY);
                    WP[pressedGame][sourcePack[pressedGame]-4].cards[i].toFront();
                }

            }
            else {
                // set new positions during dragging
                Card moveCard = ((Card)(t.getSource()));
                moveCard.setTranslateX(newTranslateX);
                moveCard.setTranslateY(newTranslateY);
                
                // dragging card will be on front of scene
                ((Card)(t.getSource())).toFront();
            }
        }
    };
  
    
    /**
     * Event if released with any card.
     */
    EventHandler<MouseEvent> cardOnMouseReleasedEventHandler = 
        new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent t) {
                                    
            int closest = closestTop(t.getSceneX(), t.getSceneY(), pressedGame);
            
            boolean flag = true;           
            // target pack
            if (closest < 4 && closest >= 0) {
                flag = TP[pressedGame][closest].put(getingCard[pressedGame]);
                // check if game is on the end
                isEndGame(pressedGame);
            }
            // working pack
            else if(closest > 3 && closest < 11) {
                if(multyGetFlag[pressedGame]) {
                    flag = WP[pressedGame][closest-4].put(getingStack[pressedGame]);
                    
                }
                else {
                    flag = WP[pressedGame][closest-4].put(getingCard[pressedGame]);
                }
                
            }
            // too far
            else {
                flag = false;
            }
            
            // give back
            // too far or bad operation
            if(flag == false) {               
                // target pack
                if (sourcePack[pressedGame] < 4 && sourcePack[pressedGame] >= 0) {
                    TP[pressedGame][sourcePack[pressedGame]].put(getingCard[pressedGame]); 
                }
                // working pack
                else if(sourcePack[pressedGame] > 3 && sourcePack[pressedGame] < 11) {
                    
                    if(multyGetFlag[pressedGame]) {
                        WP[pressedGame][sourcePack[pressedGame]-4].simplePut(getingStack[pressedGame]);
                        multyGetFlag[pressedGame] = false;
                    }
                    else {
                        WP[pressedGame][sourcePack[pressedGame]-4].simplePut(getingCard[pressedGame]);
                    }
                    
                }
                // card pack
                else if(sourcePack[pressedGame] == 11) {
                    CP[pressedGame].put(getingCard[pressedGame]); 
                }
            }
            
            multyGetFlag[pressedGame] = false;
            
            // after released refresh cards
            showStartPack(pressedGame);
            showCardPack(pressedGame);
            showTargetPack(pressedGame);
            showWorkingPack(pressedGame);
            
            // save history for undo
            if(!executeSave(pressedGame).equals(undo1[pressedGame][0])) {
                undo1[pressedGame][6] = undo1[pressedGame][5];
                undo1[pressedGame][5] = undo1[pressedGame][4];
                undo1[pressedGame][4] = undo1[pressedGame][3];
                undo1[pressedGame][3] = undo1[pressedGame][2];
                undo1[pressedGame][2] = undo1[pressedGame][1];
                undo1[pressedGame][1] = undo1[pressedGame][0];
                undo1[pressedGame][0] = executeSave(pressedGame);

                // history transfare up
                if(undo1[pressedGame][1] == null) {
                    undo1[pressedGame][1] = undo1[pressedGame][0];
                } 
            }
            
            
             hintLabels[pressedGame].setText("");
            
        }
    };
    
    /**
     * Function for execute saving.
     * @return 
     */
    String executeSave(int pack) {
        
        String cardsList = "";
        
        // target pack
        for(int i = 0; i<4; i++) {       
            if(TP[pack][i].size() != 0) {
                int j;
                for(j = 0; j < TP[pack][i].size(); j++) {
                    cardsList += (i) + "." + TP[pack][i].cards[j].color + "." + TP[pack][i].cards[j].value + 
                            "." +TP[pack][i].cards[j].turnFaceUpValue + ';';
                }          
           } 
        }
        
        // working pack
        for(int i = 0; i<7; i++) {       
            if(WP[pack][i].size() != 0) {
                int j;
                for(j = 0; j < WP[pack][i].size(); j++) {
                    cardsList += (i+4) + "." + WP[pack][i].cards[j].color + "." + WP[pack][i].cards[j].value + 
                            "." +WP[pack][i].cards[j].turnFaceUpValue + ';';
                }          
           } 
        } 
        
        // card pack
        if(CP[pack].size != 0) {
            int j;
            for(j = 0; j < CP[pack].size; j++) {
                cardsList += "11." + CP[pack].cards[j].color + "." + CP[pack].cards[j].value + 
                        "." +CP[pack].cards[j].turnFaceUpValue + ';';
            }          
        }
        
        // start pack
        if(SP[pack].size != 0) {
            int j;
            for(j = 0; j < SP[pack].size; j++) {
                cardsList += "12." + SP[pack].cards[j].color + "." + SP[pack].cards[j].value + 
                        "." +SP[pack].cards[j].turnFaceUpValue + ';';
            }          
        }
        
        return cardsList;
    }
    
    /**
     * Function for execute loading.
     * @return 
     */
    void executeLoad(String input, int pack) {
        
        // delete actual game
        Card popCard;
        int tmpSize = SP[pack].size;
        
        for(int i = 0; i < tmpSize; i++) {   
            board[pack].getChildren().remove(SP[pack].cards[i]);
            popCard = SP[pack].pop();
        }
        
        tmpSize = CP[pack].size;
        for(int i = 0; i < tmpSize; i++) {
            board[pack].getChildren().remove(CP[pack].cards[i]);    
            popCard = CP[pack].pop();
        }
        
        for(int i = 0; i<4; i++) {
            tmpSize = TP[pack][i].size();
            for(int j = 0; j < tmpSize; j++) {
                board[pack].getChildren().remove(TP[pack][i].cards[j]);
                popCard = TP[pack][i].pop();
            }
        }
        
        for(int i = 0; i<7; i++) {
            tmpSize = WP[pack][i].size();
            for(int j = 0; j < tmpSize; j++) {
                board[pack].getChildren().remove(WP[pack][i].cards[j]);
                popCard = WP[pack][i].pop();
            }
        }

        // parse information about card
        String delims = "[.;]+";
        String[] tokens = input.split(delims);
     
        int pack2;
        Card.Color color = Card.Color.SPADES;
        int value;
        boolean turnUp;
        
        for(int i=0; i < 52; i++) {
            
            pack2 = Integer.parseInt(tokens[i*4 + 0]);
            
            if(null != tokens[i*4 + 1]) switch (tokens[i*4 + 1]) {
                case "S":
                    color = Card.Color.SPADES;
                    break;
                case "D":
                    color = Card.Color.DIAMONDS;
                    break;
                case "H":
                    color = Card.Color.HEARTS;
                    break;
                case "C":
                    color = Card.Color.CLUBS;
                    break;
                default:
                    break;
            }
            
            value = Integer.parseInt(tokens[i*4 + 2]);
            turnUp = Boolean.parseBoolean(tokens[i*4 + 3]);
            
            // create new card from file
            Card newCard = factory.createCard(color, value);
            newCard.setFaceAndImage(turnUp);
            
            // put new card
            // target pack
            if (pack2 < 4 && pack2 >= 0) {
                TP[pack][pack2].put(newCard);
            }
            // working pack
            else if (pack2 > 3 && pack2 < 11) {
                WP[pack][pack2 - 4].simplePut(newCard);
            }
            // card pack
            else if (pack2 == 11) {
                CP[pack].put(newCard);
            }
            // start pack
            else if (pack2 == 12) {
                SP[pack].put(newCard);
            }
        }
        
        // show cards
        showStartPack(pack);
        showCardPack(pack);
        showTargetPack(pack);
        showWorkingPack(pack);
    }
    
    
    /**
     * Function finds the closest object.
     * Using X Y positions of objects.
     * 0-3: Target pack
     * 4-10: Working pack
     * 11: Card Pack
     * @param x X axes
     * @param y Y axes
     * @return position of the object.
     */
    public int closest(double x, double y, int pack) {
       
        // c and k is constants
        // c for x and k for y
        double c, k;
        switch (pack) {
            case 0:
                c = 0;
                k = 0;
                break;
            case 1:
                c = WINDOW_WIDTH/2;
                k = 0;
                break;
            case 2:
                c = 0;
                k = WINDOW_HEIGHT/2;
                break;
            default:
                c = WINDOW_WIDTH/2;
                k = WINDOW_HEIGHT/2;
                break;
        }
        
        
        if (x > TILE_SIZE+c && x < TILE_SIZE+CARD_WIDTH+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                
            }
            else {
                return 4;
            }
        }
        else if (x > TILE_SIZE+CARD_WIDTH+SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+2*CARD_WIDTH+SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 11;
            }
            else {
                return 5;
            }
        }
        else if (x > TILE_SIZE+2*CARD_WIDTH+2*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+3*CARD_WIDTH+2*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                
            }
            else {
                return 6;
            }
        }
        else if (x > TILE_SIZE+3*CARD_WIDTH+3*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+4*CARD_WIDTH+3*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 0;
            }
            else {
                return 7;
            }
        }
        else if (x > TILE_SIZE+4*CARD_WIDTH+4*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+5*CARD_WIDTH+4*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 1;
            }
            else {
                return 8;
            }
        }
        else if (x > TILE_SIZE+5*CARD_WIDTH+5*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+6*CARD_WIDTH+5*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 2;
            }
            else {
                return 9;
            }
        }
        else if (x > TILE_SIZE+6*CARD_WIDTH+6*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+7*CARD_WIDTH+6*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 3;
            }
            else {
                return 10;
            }
        }     
        //else
        return -1;       
    } 
    
    /**
     * Function finds the closest object.
     * Using X Y positions of objects and on their the most top card.
     * 0-3: Target pack
     * 4-10: Working pack
     * 11: Card Pack
     * @param x X axes
     * @param y Y axes
     * @return position of the object.
     */
    public int closestTop(double x, double y, int pack) {
        
        // c and k is constants
        // c for x and k for y
        double c, k;
        switch (pack) {
            case 0:
                c = 0;
                k = 0;
                break;
            case 1:
                c = WINDOW_WIDTH/2;
                k = 0;
                break;
            case 2:
                c = 0;
                k = WINDOW_HEIGHT/2;
                break;
            default:
                c = WINDOW_WIDTH/2;
                k = WINDOW_HEIGHT/2;
                break;
        }
        
        if (x > TILE_SIZE+c && x < TILE_SIZE+CARD_WIDTH+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][0].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][0].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 4;
            }
        }
        else if (x > TILE_SIZE+CARD_WIDTH+SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+2*CARD_WIDTH+SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 11;
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][1].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][1].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 5;
            }
        }
        else if (x > TILE_SIZE+2*CARD_WIDTH+2*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+3*CARD_WIDTH+2*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][2].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][2].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 6;
            }
        }
        else if (x > TILE_SIZE+3*CARD_WIDTH+3*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+4*CARD_WIDTH+3*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 0;
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][3].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][3].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 7;
            }
        }
        else if (x > TILE_SIZE+4*CARD_WIDTH+4*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+5*CARD_WIDTH+4*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 1;
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][4].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][4].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 8;
            }
        }
        else if (x > TILE_SIZE+5*CARD_WIDTH+5*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+6*CARD_WIDTH+5*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 2;
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][5].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][5].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 9;
            }
        }
        else if (x > TILE_SIZE+6*CARD_WIDTH+6*SPACE_BETWEEN_CARDS+c && x < TILE_SIZE+7*CARD_WIDTH+6*SPACE_BETWEEN_CARDS+c) {
            if(y < TOP_SIZE+CARD_HEIGHT+k && y > TOP_SIZE+k) {
                return 3;
            }
            else if(y > TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][6].size()*CARDS_UNDER-2*CARDS_UNDER+k && 
                    y < TOP_SIZE+CARD_HEIGHT+SPACE_BETWEEN_CARDS+WP[pack][6].size()*CARDS_UNDER-CARDS_UNDER+CARD_HEIGHT+k){
                return 10;
            }
        }     
        //else
        return -1;       
    } 
 
    
    /**
     * Function for check if game is on the end.
     */
    private void isEndGame(int pack) {
       
            if((TP[pack][0].size() == 13) &&
                (TP[pack][1].size() == 13) && 
                (TP[pack][2].size() == 13) &&
                (TP[pack][3].size() == 13)) {
                
                
                switch (pack) {
                    case 0:
                        if(flagBig) {
                            winHolder.setLayoutX(WINDOW_WIDTH/2 - 500/2);
                            winHolder.setLayoutY(WINDOW_HEIGHT/2 - 200/2);
                        }
                        else {
                           winHolder.setLayoutX(WINDOW_WIDTH/4 - 500/2);
                           winHolder.setLayoutY(WINDOW_HEIGHT/4 - 200/2); 
                        }
                        break;
                    case 1:
                        winHolder.setLayoutX(WINDOW_WIDTH*3/4 - 500/2);
                        winHolder.setLayoutY(WINDOW_HEIGHT/4 - 200/2);
                        break;
                    case 2:
                        winHolder.setLayoutX(WINDOW_WIDTH/4 - 500/2);
                        winHolder.setLayoutY(WINDOW_HEIGHT*3/4 - 200/2);
                        break;
                    case 3:
                        winHolder.setLayoutX(WINDOW_WIDTH*3/4 - 500/2);
                        winHolder.setLayoutY(WINDOW_HEIGHT*3/4 - 200/2);
                        break;
                    default:
                        break;
                }
                
                winHolder.setVisible(true); 
                winHolder.toFront();  
            }          
    }
    
    /**
     * Function for showing all cards from start pack.
     */
    private void showStartPack(int pack) {

        if(SP[pack].size != 0) {
            // show more cards on its own
            for(int j = 0; j < SP[pack].size; j++) {
                Card top = SP[pack].get(j);                   
                interpretStartPackCard(top, SPGround[pack].getLayoutX(), SPGround[pack].getLayoutY(), pack);
            }
        }        
    }
    
    /**
     * Function for showing all cards from card pack.
     */
    private void showCardPack(int pack) {

        SPGround[pack].setOnMousePressed(pressReturn);
        SPGround[pack].setCursor(Cursor.HAND);
        
        if(CP[pack].size != 0) {
                Card top = CP[pack].get();  
                top.setFaceAndImage(true);
                interpretCard(top, CPGround[pack].getLayoutX(), CPGround[pack].getLayoutY(), pack);   
        }
    }
    
    /**
     * Function for showing all cards from all target packs.
     * The function shows more cards on its own, because the second top card 
     * must be seen when pops the top card.
     */
    private void showTargetPack(int pack) {
           
        for(int i = 0; i < 4; i++) {        
        
            if(TP[pack][i].size() != 0) {
                
                // show more cards on its own
                for(int j = 0; j < TP[pack][i].size(); j++) {
                             
                    Card top = TP[pack][i].get(j);
                    top.setFaceAndImage(true);
                    interpretCard(top, TPGround[pack][i].getLayoutX(), TPGround[pack][i].getLayoutY(), pack);
                }                 
            }     
        }
    }
    
    /**
     * Function for showing all cards from all working packs.
     */
    private void showWorkingPack(int pack) {

        for(int i = 0; i < 7; i++) {
            
            if(WP[pack][i].size() != 0) {
                int j;
                for(j = 0; j < WP[pack][i].size()-1; j++) {
                             
                    Card top = WP[pack][i].get(j);   
                    interpretCard(top, WPGround[pack][i].getLayoutX(), WPGround[pack][i].getLayoutY() + j*CARDS_UNDER, pack);
                }
                Card top = WP[pack][i].get(j);
                top.setFaceAndImage(true);
                interpretCard(top, WPGround[pack][i].getLayoutX(), WPGround[pack][i].getLayoutY() + j*CARDS_UNDER, pack);
            }            
        }
    }

    /**
     * Interpret card function for start pack cards.
     * @param c
     * @param x
     * @param y 
     */
    private void interpretStartPackCard(Card c, double x, double y, int pack) {
        
        if (board[pack].getChildren().contains(c)) {
            board[pack].getChildren().remove(c);
        }
            
        c.setFaceAndImage(c.isTurnedFaceUp());

        board[pack].getChildren().add(c);

        // set card for mouse events
        c.setOnMousePressed(pressStartPackCard);
        c.setCursor(Cursor.HAND);
              
        c.setLayoutX(x);
        c.setLayoutY(y);        
    }


    /**
     * Interpret card.
     * @param c card
     * @param x 
     * @param y 
     */
    private void interpretCard(Card c, double x, double y, int pack) {
        
        
        // if exists remove
        if (board[pack].getChildren().contains(c)) {
            board[pack].getChildren().remove(c);
        }
        
        // add card on the board[0] and set face
        c.setFaceAndImage(c.isTurnedFaceUp());
        board[pack].getChildren().add(c);
        
        // if card is turned face up
        if (c.isTurnedFaceUp()) {
            // set card for mouse events
            c.setOnMousePressed(cardOnMousePressedEventHandler);
            c.setOnMouseDragged(cardOnMouseDraggedEventHandler);
            c.setOnMouseReleased(cardOnMouseReleasedEventHandler);
            c.setCursor(Cursor.HAND);
        }
        
        // set position      
        c.setLayoutX(x);
        c.setLayoutY(y);        
    }
    
    /**
     * Initialize one start pack.
     */
    private void initStartPack(int pack) {
        
        // if exists 
        if (!board[pack].getChildren().contains(SPGround[pack])) {
            Ground ground = new Ground(4);
            SPGround[pack] = ground;
            board[pack].getChildren().add(ground);
            ground.setLayoutX(TILE_SIZE);
            ground.setLayoutY(TOP_SIZE); 
        }
        
        SP[pack] = new StartPack();
        SP[pack] = factory.startInitialPack();       
    }
    
    /**
     * Initialize one card pack.
     */
    private void initCardPack(int pack) {
        // if exists 
        if (!board[pack].getChildren().contains(CPGround[pack])) {
            Ground ground = new Ground();
            CPGround[pack] = ground;
            board[pack].getChildren().add(ground);
            ground.setLayoutX(TILE_SIZE + CARD_WIDTH + SPACE_BETWEEN_CARDS);
            ground.setLayoutY(TOP_SIZE);
            
        }
        CP[pack] = new CardPack();
        CP[pack] = factory.createCardPack();
    }
 
    /**
     * Initialize array (4) of target pack.
     */
    private void initTargetPack(int pack) {
                
        int constant = 0; //for GUI       
        int i = 0;
        for(int x = 3; x < 7; x++) {  //bad value for GUI  
            
            // if exists 
            if (!board[pack].getChildren().contains(TPGround[pack][i])) {
                // GUI part
                Ground ground = new Ground();
                TPGround[pack][i] = ground;
                board[pack].getChildren().add(ground);
                ground.setLayoutX(TILE_SIZE  + x * CARD_WIDTH + 3*SPACE_BETWEEN_CARDS + constant);
                constant += SPACE_BETWEEN_CARDS;
                ground.setLayoutY(TOP_SIZE); 
            }
            
            // logic part
            // set what type (target color) of target will be created
            CardDeck targetPack = new CardDeck();

            switch (i) {
                    case 0:
                        targetPack = factory.createTargetPack(Card.Color.SPADES);
                        break;
                    case 1:
                        targetPack = factory.createTargetPack(Card.Color.DIAMONDS);
                        break;
                    case 2:
                        targetPack = factory.createTargetPack(Card.Color.HEARTS);
                        break;
                    case 3:
                        targetPack = factory.createTargetPack(Card.Color.CLUBS);
                        break;
                    default:
                        break;
                }

            TP[pack][i]= targetPack;

            i++;  
        }              
    }

    /**
     * Initialize array (4) of working pack.
     */
    private void initWorkingPack(int pack) {
        
        int constant = 0; // for GUI
        
        int i = 0;
        for(int x = 0; x < 7; x++) {
                
            // if exists 
            if (!board[pack].getChildren().contains(WPGround[pack][i])) {          
                // GUI part
                Ground ground = new Ground();
                WPGround[pack][i] = ground;   
                board[pack].getChildren().add(ground);
                ground.setLayoutX(TILE_SIZE  + x * CARD_WIDTH + constant);
                constant += SPACE_BETWEEN_CARDS;
                ground.setLayoutY(TOP_SIZE + CARD_HEIGHT + SPACE_BETWEEN_CARDS);          
            }
            
            // logic part
            CardStack workingPack = factory.createWorkingPack();
            WP[pack][i]= workingPack;
            
            i++; 
        }  
        
        // set cards to working packs
        Card top;
        for(int c = 0; c < 7; c++) {
            for(int j = 0; j < c; j++) {
                top = SP[pack].pop();               
                WP[pack][c].simplePut(top);
            }
            top = SP[pack].pop();
            top.setFaceAndImage(true);
            WP[pack][c].simplePut(top);  
        }    
    }
    
    /**
     * configure window and start application
     * @param primaryStage Main stage for JavaFX
     * @throws Exception Exception for JavaFX application
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        Scene scene = new Scene(createContent(), WINDOW_WIDTH, WINDOW_HEIGHT);
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setTitle("Solitaire");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();   
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
