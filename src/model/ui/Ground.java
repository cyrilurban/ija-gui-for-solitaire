/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ui;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

import static model.ui.Solitaire.CARD_WIDTH;
import static model.ui.Solitaire.CARD_HEIGHT;



/**
 *
 * @author cyril
 */
public class Ground extends StackPane {
    
    public Image image;
    public ImageView view;
    
    public Ground() {
        
        this.image = new Image("file:lib/zero.png");
        
        view = new ImageView(this.image);  
        view.setFitHeight(CARD_HEIGHT);
        view.setFitWidth(CARD_WIDTH);
        getChildren().addAll(view);

    }
    
    public Ground(int i){
        
        switch (i) {
            case 4:
                this.image = new Image("file:lib/return.png");
                break;
            default:
                break;
        }
        
        view = new ImageView(this.image);       
        view.setFitHeight(CARD_HEIGHT);
        view.setFitWidth(CARD_WIDTH);
        getChildren().addAll(view);
    }


}
