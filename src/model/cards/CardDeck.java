/**
 * @file    CardDeck.java
 * @author  CYRIL URBAN
 * @date    2017-03-01
 * @brief   The class CardDeck
 */

package model.cards;

/**
 * Class for card deck.
 */
public class CardDeck {

    public Card.Color deckColor;

    public Card[] cards = new Card[14];
    public int size = 0;

    /**
     * Constructs the object and set color.
     *
     * @param color Color of card.
     */
    public CardDeck(Card.Color color) {
        deckColor = color;
    }

    /**
     * Constructs the object.
     */
    public CardDeck() {
    }

    /**
     * Constructs the object.
     *
     * @param size The size
     */
    public CardDeck(int size) {
        this.size = size;
    }

    /**
     * Factory method, which creates a package of 52 cards. 13 cards (values 1
     * to 13) for each color.
     *
     * @return Instance of the class.
     */
    public CardDeck createStandardDeck() {

        CardDeck deck = new CardDeck();
        int position = 0;
        Card.Color color = Card.Color.SPADES;

        for (int i = 1; i < 14; i++) {
            for (int j = 0; j < 4; j++) {

                switch (j) {
                    case 0:
                        color = Card.Color.SPADES;
                        break;
                    case 1:
                        color = Card.Color.DIAMONDS;
                        break;
                    case 2:
                        color = Card.Color.HEARTS;
                        break;
                    case 3:
                        color = Card.Color.CLUBS;
                        break;
                    default:
                        break;
                }

                cards[position] = new Card(color, i);
                position++;
            }
        }

        size = 52;
        return deck;
    }

    /**
     * get size
     *
     * @return size
     */
    public int size() {
        return this.size;
    }

    /**
     * Put the card on the deck.
     * Put is possible if putting card has the same color as card from 
     * the top of the deck (except the first) and putting card has a value +1 
     * than card from the top of the deck.
     * 
     * @param card The card
     * @return true || false
     */
    public boolean put(Card card) {
        
        // get card from the top of the deck
        Card getCard = get();

        if (getCard == null) {
            // first card
            if ((card.value() == 1)) {
                deckColor = card.color();
                // add card on the top of the deck
                cards[size] = new Card(card.color(), card.value());
                this.size++;
                return true;
            // bad first card
            } else {
                return false;
            }
        } else {
            // next card
            if ((card.compareValue(getCard) == 1) && (card.color.toString().equals(getCard.color.toString()))) {
                // add card on the top of the deck
                cards[size] = new Card(card.color(), card.value());
                this.size++;
                return true;
            // bad card
            } else {
                return false;
            }
        }
    }

    /**
     * remove the card from the top of the stack
     *
     * @return card from the top of the deck
     */
    public Card pop() {

        if (this.size == 0) {
            return null;
        } else {
            Card popCart = new Card(cards[size - 1].color(), cards[size - 1].value());
            this.size--;
            return popCart;
        }
    }

    /**
     * Test if card deck is empty.
     *
     * @return True or false
     */
    public boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }

    /**
     * Function returns the card from the top of the deck or NULL if deck is
     * empty.
     *
     * @return card || NULL
     */
    public Card get() {
        // empty == NULL
        if (isEmpty()) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[size - 1];
        return getCard;
    }

    /**
     * Function returns the card from the index of the deck or NULL if deck is
     * empty or out of range.
     * 
     * @param index Index of card in Deck.
     * @return card || NULL
     */
    public Card get(int index) {
        // empty or out of range == NULL
        if (isEmpty() || index > size) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[index];
        return getCard;
    }
}
