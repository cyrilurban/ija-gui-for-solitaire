/**
 * @file    Card.java
 * @author  CYRIL URBAN, VIT SOLCIK
 * @date    2017-03-01
 * @brief   The class Card
 */

package model.cards;

public class CardPack {

    public static Card[] cards = new Card[25];
    public int size = 0;
    
    
    /**
     * Constructor
     */
    public CardPack() {
    }

    /**
     * Size of pack
     *
     * @param size The size
     */
    public CardPack(int size) {
        this.size = size;
    }

    /**
     * test if pack is empty
     *
     * @return True if empty, False otherwise.
     */
    public boolean isEmpty() {
        if (this.size <= 0) {
            return true;
        }
        return false;
    }

    /**
     * Function returns the card from the top of the deck or NULL if deck is
     * empty.
     *
     * @return card || NULL
     */
    public Card get() {
        // empty == NULL
        if (isEmpty()) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[size - 1];
        return getCard;
    }
    
    
    /**
     * remove (and read) the card from top of the stack
     *
     * @return Card popCard
     */
    public Card pop() {

        if (this.size == 0) {
            return null;
        } else {
            cards[size] = null;
            Card popCard = new Card(cards[size - 1].color(), cards[size - 1].value());
            this.size--;
            return popCard;
        }
    }

    /**
     * Put card in starter pack
     *
     * @param      card  The card
     *
     * @return     true || false
     */
    public boolean put (Card card)  {
        
    if (this.size <= 52) {
        cards[size] = new Card(card.color(), card.value());
        cards[size].turnFaceUpValue = card.turnFaceUpValue;
        this.size++;
        return true;
    }
    else
        return false;
    
    }


}
