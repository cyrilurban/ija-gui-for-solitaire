/**
 * @file    Card.java
 * @author  CYRIL URBAN
 * @date    2017-03-01
 * @brief   The class Card
 */

package model.cards;

//import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import static model.ui.Solitaire.CARD_HEIGHT;
import static model.ui.Solitaire.CARD_WIDTH;

/**
 * The class represents one card. The card contains information about its value
 * (1-13) and color.
 */
public class Card extends StackPane {
    
    public Image image;
    public Image backImage;
    public ImageView view;
            
    public int value;
    public Color color;
    public boolean turnFaceUpValue = false;

    /**
     * Tests whether cards have a similar color (red or black).
     *
     * @param c Input card
     * @return true || false
     */
    public boolean similarColorTo(Card c) {

        // true if colors:
        // (SPADES || CLUBS) equals with (SPADES || CLUBS)
        if ((c.color.toString().equals(("S")) || (c.color.toString().equals("C")))) {
            if ((this.color.toString().equals(("S")) || (this.color.toString().equals("C")))) {
                return true;
            }
        } // (DIAMONDS || HEARTS) equals with (DIAMONDS || HEARTS)
        else if ((c.color.toString().equals(("D")) || (c.color.toString().equals("H")))) {
            if ((this.color.toString().equals(("D")) || (this.color.toString().equals("H")))) {
                return true;
            }
        }
        // else false
        return false;
    }

    /**
     * Compares values of the cards. Returns difference of cards values.
     *
     * @param c Input card
     * @return difference of cards values
     */
    public int compareValue(Card c) {
        return this.value - c.value;
    }

    /**
     * Tests whether the card turned face up.
     *
     * @return true || false
     */
    public boolean isTurnedFaceUp() {
        if (turnFaceUpValue) {
            return true;
        } else {
            return false;
        }
    }

    
    /**
     * Set face value and set image.
     * @param up True for front, false for back of card
     */
    public void setFaceAndImage(boolean up) {
        if(up){
            turnFaceUpValue = true;
            view.setImage(this.image);
        }
        else {
            turnFaceUpValue = false;
            view.setImage(this.backImage);
        }
    }
 
    
    /**
     * Turns the card face up. If it is face up, do nothing.
     *
     * @return true (The card was turned) || false
     */
    public boolean turnFaceUp() {
        if (turnFaceUpValue) {
            this.turnFaceUpValue = false;
            return false;
        } else {
            this.turnFaceUpValue = true;
            return true;
        }
    }

    /**
     * constructor constructor sets value and color
     *
     * @param c color
     * @param value The value
     */
    public Card(Card.Color c, int value) {
            
        this.value = value;
        this.color = c;
        
        this.image = new Image("file:lib/" + this.toString() + ".png");
        this.backImage = new Image("file:lib/back.png");
        

        if(isTurnedFaceUp() == true) {
            view = new ImageView(this.image);
        }
        else {
            view = new ImageView(this.backImage);
        }
        
        view.setFitHeight(CARD_HEIGHT);
        view.setFitWidth(CARD_WIDTH);
        
        getChildren().addAll(view);
                
    }   


    
    /**
     * get color color of the card
     *
     * @return color
     */
    public Color color() {
        return color;
    }

    /**
     * get value value of the card
     *
     * @return value
     */
    public int value() {
        return value;
    }

    /**
     * enumerator Color
     */
    public enum Color {
        SPADES,
        DIAMONDS,
        HEARTS,
        CLUBS;

        @Override
        public String toString() {
            switch (this) {
                case SPADES:
                    return "S";
                case DIAMONDS:
                    return "D";
                case HEARTS:
                    return "H";
                case CLUBS:
                    return "C";
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

    /**
     * Transformation the value and color of the card into a string
     *
     * @return string
     */
    @Override
    public String toString() {

        // card value is a number
        if ((value > 1) && (value < 11)) {
            return (value + "(" + color.toString() + ")");
        } // card value is not a number - is a picture (etc 11 = Jack = J)
        else {
            String picture = "\0";
            switch (value) {
                case 11:
                    picture = "J";
                    break;
                case 12:
                    picture = "Q";
                    break;
                case 13:
                    picture = "K";
                    break;
                case 1:
                    picture = "A";
                    break;
                default:
                    break;
            }

            return (picture + "(" + color.toString() + ")");
        }
    }

    /**
     * hashCode
     *
     * @return result
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + value;
        return result;
    }

    /**
     * equals
     *
     * @param obj The object
     *
     * @return true/false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Card other = (Card) obj;
        if (color != other.color) {
            return false;
        }
        if (value != other.value) {
            return false;
        }
        return true;
    }
}
