/**
 * @file    Card.java
 * @author  CYRIL URBAN, VIT SOLCIK
 * @date    2017-03-01
 * @brief   The class Card
 */

package model.cards;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class StartPack {
    public static Card[] cards = new Card[52];
    public static int size = 0;
    
    
    /**
     * Constructor
     */
    public StartPack() {
    }

    /**
     * Size of pack
     *
     * @param size The size
     */
    public StartPack(int size) {
        this.size = size;
    }

    /**
     * test if pack is empty
     *
     * @return True if empty, False otherwise.
     */
    public boolean isEmpty() {
        if (size <= 0) {
            return true;
        }
        return false;
    }

    /**
     * remove (and read) the card from top of the stack
     *
     * @return Card popCard
     */
    public Card pop() {

        if (this.size == 0) {
            return null;
        } else {
            Card popCard = new Card(cards[size - 1].color(), cards[size - 1].value());
            this.size--;
            return popCard;
        }
    }

    /**
     * Put card in starter pack
     *
     * @param      card  The card
     *
     * @return     true || false
     */
    public boolean put (Card card)  {
        
    if (this.size <= 52) {
        cards[size] = new Card(card.color(), card.value());
        this.size++;
        return true;
    }
    else
        return false;
    
    }

    /**
     * Function returns the card from the top of the deck or NULL if deck is
     * empty.
     *
     * @return card || NULL
     */
    public Card get() {
        // empty == NULL
        if (isEmpty()) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[size - 1];
        return getCard;
    }
    

    /**
     * Function returns the card from the index of the deck or NULL if deck is
     * empty or out of range.
     * 
     * @param index Index of card in Pack.
     * @return card || NULL
     */
    public Card get(int index) {
        // empty or out of range == NULL
        if (isEmpty() || index > this.size) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[index];
        return getCard;
    }

    /**
     * Factory method, which creates a package of 52 cards. 13 cards (values 1
     * to 13) for each color randomly.
     *
     * @return Instance of the class.
     */
    public static StartPack createStartPack() {

        StartPack pack = new StartPack();
        int position = 0;
        Card.Color color = Card.Color.SPADES;
     
        for (int i = 1; i < 14; i++) {
            for (int j = 0; j < 4; j++) {

                switch (j) {
                    case 0:
                        color = Card.Color.SPADES;
                        break;
                    case 1:
                        color = Card.Color.DIAMONDS;
                        break;
                    case 2:
                        color = Card.Color.HEARTS;
                        break;
                    case 3:
                        color = Card.Color.CLUBS;
                        break;
                    default:
                        break;
                }

                cards[position] = new Card(color, i);
                position++;
            }
        }

        Random rnd = ThreadLocalRandom.current();
        for (int i = cards.length - 1; i > 0; i--)
        {
          int index = rnd.nextInt(i + 1);
          Card a = cards[index];
          cards[index] = cards[i];
          cards[i] = a;
        }
        
        size = 52;
        return pack;
    }

}
