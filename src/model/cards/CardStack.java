/**
 * @file    CardStack.java
 * @author  CYRIL URBAN
 * @date:   2017-03-01
 * @brief   The class Card
 */
package model.cards;

import java.util.Arrays;

/**
 * The class represents a stack of cards
 */
public class CardStack {

    public Card[] cards = new Card[21];
    public int size = 0;
    public int top = 0;

    /**
     * Constructor
     */
    public CardStack() {
    }

    /**
     * Constructor
     *
     * @param size The size
     */
    public CardStack(int size) {
        this.size = size;
    }

    /**
     * test if stack is empty
     *
     * @return True if empty, False otherwise.
     */
    public boolean isEmpty() {
        if (top <= 0) {
            return true;
        }
        return false;
    }

    /**
     * remove (and read) the card from top of the stack
     *
     * @return Card popCard
     */
    public Card pop() {

        if (this.top == 0) {
            return null;
        } else {
            Card popCard = new Card(cards[top - 1].color(), cards[top - 1].value());
            popCard.turnFaceUpValue = cards[top - 1].turnFaceUpValue;
            this.top--;
            return popCard;
        }
    }

    /**
     * Put cards from the stack on top of the stack
     *
     * @param card The card
     * @return True || False
     */
    public boolean put(Card card) {

        // get card from the top of the deck
        Card getCard = get();

        if (getCard == null) {
            // first card - king
            if (card.value() == 13) {
                // OK, put
                cards[top] = new Card(card.color(), card.value());
                this.top++;
                if (top > size) {
                    this.size++;
                }
                return true;
            } else {
                return false;
            }
        } else {
            // next card
            if ((card.compareValue(getCard) == -1) && (card.similarColorTo(getCard) == false)) {
                // OK, put
                cards[top] = new Card(card.color(), card.value());
                this.top++;
                if (top > size) {
                    this.size++;
                }
                return true;
                // bad card
            } else {
                return false;
            }
        }
    }

    /**
     * Put cards from the stack on top of the stack
     *
     * @param card The card
     */
    public void simplePut(Card card) {

        cards[top] = new Card(card.color(), card.value());
        cards[top].turnFaceUpValue = card.turnFaceUpValue;
        this.top++;

        if (top > size) {
            this.size++;
        }
    }

    /**
     * Help function for back putting.
     * @param stack Stack for insertion/putting.
     */
    public void simplePut(CardStack stack) {
        CardStack tmpStack = new CardStack(stack.top);
        
        // pop part 1/2. From stack to tmp stack
        while (stack.top != 0) {
            Card popCard = stack.pop();
            tmpStack.simplePut(popCard);
        }
        // pop part 2/2 (the correct order). From stack to this stack
        while (tmpStack.top != 0) {
            Card popCard = tmpStack.pop();
            this.simplePut(popCard);
        }
    }
    
    /**
     * Puts the card on the stack. if: goal stack is empty and card from top of
     * source stack is king or if goal stack is not empty and is -1 and color is
     * not similar (put can be provided - return true) Else: put can not be
     * provided, do nothing and return false.
     *
     * @param stack The stack
     * @return True || False
     */
    public boolean put(CardStack stack) {

        CardStack tmpStack = new CardStack(stack.top);

        // if goal stack is empty and 
        // card from top of source stack must be king (value 13) 
        if ((this.get() == null) && (stack.get(0).value() == 13)) {
            // pop part 1/2. From stack to tmp stack
            while (stack.top != 0) {
                Card popCard = stack.pop();
                tmpStack.simplePut(popCard);
            }
            // pop part 2/2 (the correct order). From stack to this stack
            while (tmpStack.top != 0) {
                Card popCard = tmpStack.pop();
                this.simplePut(popCard);
            }
            return true;
            // if goal stack is not empty 
            // and compare must be -1
            // and color mustn't be similar 
        } else if ((this.get() != null)
                && (stack.get(0).compareValue(this.get()) == -1)
                && (stack.get(0).similarColorTo(this.get()) == false)) {

            // pop part 1/2. From stack to tmp stack
            while (stack.top != 0) {
                Card popCard = stack.pop();
                tmpStack.simplePut(popCard);
            }
            // pop part 2/2 (the correct order). From stack to this stack
            while (tmpStack.top != 0) {
                Card popCard = tmpStack.pop();
                this.simplePut(popCard);
            }
            return true;
        }
        // else false
        return false;
    }

    /**
     * get size (current top)
     *
     * @return size
     */
    public int size() {
        return this.top;
    }

    /**
     * The method removes sequence of the cards from the stack up to the top of
     * the stack.
     *
     * @param card The card
     *
     * @return returnStack
     */
    public CardStack pop(Card card) {

        int count = 0;
        CardStack returnStack = new CardStack(0);
        CardStack tmpStack = new CardStack(0);

        // put cards to tmpStack
        // first pop
        Card popCard = this.pop();
        tmpStack.simplePut(popCard);
        count++;
        // the other pops
        while ((popCard.equals(card)) == false) {

            popCard = this.pop();
            if (popCard == null) {
                // back
                this.top += count;
                return null;
            }

            tmpStack.simplePut(popCard);
            count++;
        }
        // put cards to returnStack
        for (int i = 0; i < count; i++) {

            popCard = tmpStack.pop();
            returnStack.simplePut(popCard);
        }

        // change the value of size (=top) of stack
        returnStack.size = count;
        returnStack.top = count;

        return returnStack;
    }

    /**
     * Function returns the card from the top of the deck or NULL if deck is
     * empty.
     *
     * @return card || NULL
     */
    public Card get() {
        // empty == NULL
        if (isEmpty()) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[top - 1];
        return getCard;
    }

    /**
     * Function returns the card from the index of the deck or NULL if deck is
     * empty or out of range.
     *
     * @param index Index of card in Stack.
     * @return card || NULL
     */
    public Card get(int index) {
        // empty or out of range == NULL
        if (isEmpty() || index > size) {
            return null;
        }
        // else card from the top of the deck
        Card getCard = cards[index];
        return getCard;
    }

    /**
     * hashCode
     *
     * @return result
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(cards);
        result = prime * result + size;
        result = prime * result + top;
        return result;
    }

    /**
     * equals
     *
     * @param obj The object
     *
     * @return true/false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CardStack other = (CardStack) obj;
        if (!Arrays.equals(cards, other.cards)) {
            return false;
        }
        if (size != other.size) {
            return false;
        }
        if (top != other.top) {
            return false;
        }
        return true;
    }
}
