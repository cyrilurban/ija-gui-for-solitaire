/**
 * @file    Card.java
 * @author  CYRIL URBAN
 * @date    2017-03-22
 * @brief   The class FactoryKlondike
 */

package model.board;
import model.cards.Card;
import model.cards.CardDeck;
import model.cards.CardPack;
import model.cards.CardStack;
import model.cards.StartPack;

/**
 * Class implements a specific factory for creating instances.
 */
public class FactoryKlondike extends AbstractFactorySolitaire {

    /**
     * Constructs the object.
     */
    public FactoryKlondike() {
    }

    /**
     * Function checks validity of parameters and creates a new card
     *
     * @param color Color of card
     * @param value Value of card (int)
     * @return Card
     */
    @Override
    public Card createCard(Card.Color color, int value) {
        // check validity of params
        if (value < 1 || value > 13) {
            return null;
        }
        // create new card and return
        Card newCard = new Card(color, value);
        return newCard;
    }

    /**
     * Creates an object representing a deck of cards.
     *
     * @return Card deck
     */
//    @Override
//    public CardDeck createCardDeck() {
//        CardDeck newCardDeck;
//        newCardDeck = CardDeck.createStandardDeck();
//        return newCardDeck;
//    }

    /**
     * Creates an object representing the target deck of cards.
     *
     * @param color Color of card
     * @return Target deck
     */
    @Override
    public CardDeck createTargetPack(Card.Color color) {
        CardDeck newTargetPack = new CardDeck(color);
        return newTargetPack;
    }

    /**
     * Creates an object representing the working field for the card.
     * 
     * @return Card stack
     */
    @Override
    public CardStack createWorkingPack() {
        CardStack newCardStack = new CardStack();
        return newCardStack;
    }

    /**
     * Creates an object representing the start field for the card.
     * 
     * @return Card start pack
     */
    @Override
    public StartPack startInitialPack() {
       StartPack newStartPack;
       newStartPack = StartPack.createStartPack();
       return newStartPack;
    }

    /**
     * Creates an object representing the start field for the card.
     * 
     * @return Card start pack
     */
    @Override
    public CardPack createCardPack() {
        CardPack newCardPack = new CardPack();
        return newCardPack;
    }    
}
