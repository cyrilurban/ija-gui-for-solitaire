/**
 * @file    Card.java
 * @author  CYRIL URBAN
 * @date    2017-03-22
 * @brief   The class AbstractFactorySolitaire
 */

package model.board;
import model.cards.Card;
import model.cards.CardDeck;
import model.cards.CardPack;
import model.cards.CardStack;
import model.cards.StartPack;

/**
 * Abstract class defines a specific factory for creating instances.
 */
public abstract class AbstractFactorySolitaire {

//    public abstract CardDeck createCardDeck();
    public abstract Card createCard(Card.Color color, int value);
    public abstract CardDeck createTargetPack(Card.Color color);
    public abstract CardStack createWorkingPack();
    public abstract StartPack startInitialPack();
    public abstract CardPack createCardPack();

}
